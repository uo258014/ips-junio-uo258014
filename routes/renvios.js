const lib = require('../modules/lib.js');

module.exports = function (app, swig, gestorBD) {

    app.get('/seguimiento', function (req, res) {
        let criterio;
        let params = [];
        let search = req.query.busqueda;
        if (search != "" && search != null) {
            criterio = {
                code: search
            };
            gestorBD.obtenerEnvios(criterio, function (envios) {
                if (envios.length == 1) {
                    params['envio'] = envios[0];
                    params['estados'] = envios[0].state;
                    params['search'] = true;
                    res.send(lib.globalRender('views/bseguimiento.html', params, req.session));
                } else {
                    res.redirect("/seguimiento?mensaje=No hay envíos para ese código&tipoMensaje=alert-danger")
                }
            });
        } else
            res.send(lib.globalRender('views/bseguimiento.html', params, req.session));
    });

    app.get('/transportista', function (req, res) {
        let criterio;
        let params = [];
        gestorBD.obtenerTransportistas({}, function (transportistas) {
            params['transportistas'] = transportistas;
            if (req.query.transportista != "" && req.query.transportista != null) {

                criterio = {transport: gestorBD.mongo.ObjectID(req.query.transportista)};
                gestorBD.obtenerEnvios(criterio, function (envios) {
                    params['envios'] = envios;
                    params['search'] = true;
                    res.send(lib.globalRender('views/btransportista.html', params, req.session));
                });
            } else {
                params['search'] = false;
                res.send(lib.globalRender('views/btransportista.html', params, req.session));
            }
        });
    });

    app.get('/envio/actualizar/:code', function (req, res) {
        let params = [];
        let Ecode = req.params.code;
        gestorBD.obtenerEnvios({code: Ecode}, function (envios) {
            let e = envios[0].state;
            params['estados'] = e;
            params['envio'] = envios[0];
            res.send(lib.globalRender('views/bactualizar.html', params, req.session));
        });
    });

    app.get('/envio/ruta/:code', function (req, res) {
        let params = [];
        let Ecode = req.params.code;
        gestorBD.obtenerEnvios({code: Ecode}, function (envios) {
            let e = envios[0].state;
            params['estados'] = e;
            params['envio'] = envios[0];
            res.send(lib.globalRender('views/broute.html', params, req.session));
        });
    });

    app.get('/editar', function (req, res) {
        let criterio;
        let params = [];
        let search = req.query.busqueda;
        if (search != "" && search != null) {
            criterio = {
                code: search
            };
            gestorBD.obtenerEnvios(criterio, function (envios) {
                if (envios.length == 1) {
                    if (envios[0].state[envios[0].state.length - 1].size < 8) {
                        if (envios[0].state[envios[0].state.length - 1].size != 7 || !envios[0].sendToOffice) {
                            params['envio'] = envios[0];
                            params['estados'] = envios[0].state;
                            params['search'] = true;
                            if (envios[0].state[envios[0].state.length - 1].size < 6) {
                                params['province'] = true;
                            }
                            res.send(lib.globalRender('views/beditar.html', params, req.session));
                        } else {
                            res.redirect("/editar?mensaje=Su envío ya se encuentra en la oficina de destino&tipoMensaje=alert-danger")
                        }
                    } else
                        res.redirect("/editar?mensaje=Los envíos que han salido de la oficina de destino hacia su domicilio, o han sido entregados, no pueden ser editados&tipoMensaje=alert-danger")
                } else {
                    res.redirect("/editar?mensaje=No hay envíos para ese código&tipoMensaje=alert-danger")
                }
            });
        } else
            res.send(lib.globalRender('views/beditar.html', params, req.session));
    });

    app.post('/editar/:code', function (req, res) {
        let params = [];
        let Ecode = req.params.code;
        let criterio = {
            code: Ecode
        };
        gestorBD.obtenerEnvios(criterio, function (envios) {
            let envio = envios[0];
            let e;
            if (req.body.direccion == "" || req.body.direccion == null)
                envio.sendToOffice = true;
            else
                envio.sendToOffice = false;
            envio.direction = req.body.direccion;
            if (req.body.provincia != "" && req.body.provincia != null) {
                envio.province = req.body.provincia;
                e = {
                    name: "Modificación del envío, nuevos datos:  Dirección: " + req.body.direccion + " Provincia: " + req.body.provincia + lib.parseDate(new Date(Date.now())),
                    size: envio.state[envio.state.length - 1].size
                };
            } else {
                e = {
                    name: "Modificación del envío, nuevos datos:  Dirección: " + req.body.direccion + lib.parseDate(new Date(Date.now())),
                    size: envio.state[envio.state.length - 1].size
                };
            }
            envio.state.push(e);
            gestorBD.actualizarEnvio({code: Ecode}, envio, function (result) {
                if (result == null)
                    res.redirect("/editar?mensaje=Error del servidor&tipoMensaje=alert-danger");
                else {
                    params['envio'] = envio;
                    res.redirect("/editar?mensaje=Envio actualizado correctamente&tipoMensaje=alert alert-success");
                }
            });
        });
    });


    app.get('/cambiarEstado/:code', function (req, res) {
        let Ecode = req.params.code;
        let input = req.query.estado;
        let estados = [];
        gestorBD.obtenerEnvios({code: req.params.code}, function (envios) {
            estados = envios[0].state;
            if (input == "recogidoDomicilio") {
                let e = {
                    name: "Recogido en domicilio " + lib.parseDate(new Date(Date.now())),
                    size: 2
                };
                estados.push(e);
            } else if (input == "depositadoOficinaSalida") {
                let e = {
                    name: "En oficina de origen " + lib.parseDate(new Date(Date.now())),
                    size: 3
                };
                estados.push(e);
            } else if (input == "recogidoOficinaSalida") {
                let e = {
                    name: "Salida de oficina de origen " + lib.parseDate(new Date(Date.now())),
                    size: 4
                };
                estados.push(e);
            } else if (input == "entregadoOficinaCentral") {
                let e = {
                    name: "Llegada a oficina central " + lib.parseDate(new Date(Date.now())),
                    size: 5
                };
                estados.push(e);
            } else if (input == "recogidoOficinaCentral") {
                let e = {
                    name: "Salida de oficina central " + lib.parseDate(new Date(Date.now())),
                    size: 6
                };
                estados.push(e);
            } else if (input == "entregadoOficinaDestino") {
                let e;
                if (envios[0].sendToOffice) {
                    e = {
                        name: "Entregado en la oficina de destino (" + envios[0].province + ") Debe pasar a recogerlo " + lib.parseDate(new Date(Date.now())),
                        size: 12
                    };
                } else {
                    e = {
                        name: "Entregado en la oficina de destino (" + envios[0].province + ") " + lib.parseDate(new Date(Date.now())),
                        size: 7
                    };
                }
                estados.push(e);
            } else if (input == "recogidoOficinaDestino") {
                let e = {
                    name: "Recogido en la oficina de destino (" + envios[0].province + ") " + lib.parseDate(new Date(Date.now())),
                    size: 8
                };
                estados.push(e);
            } else if (input == "intentoEntrega") {
                let e = {
                        name: "Se intentó realizar la entrega en " + envios[0].direction + " pero el destinatario estaba ausente " + lib.parseDate(new Date(Date.now())),
                        size: envios[0].state[envios[0].state.length - 1].size + 1
                    }
                ;
                estados.push(e);
            } else if (input == "entregaFallida") {
                let e = {
                    name: "3 intentos de entrega fallidos, será depositado en oficina destino (" + envios[0].province + ") " + lib.parseDate(new Date(Date.now())),
                    size: 12
                };
                estados.push(e);
            } else if (input == "entregado") {
                let e = {
                    name: "El paquete fué entregado correctamente en la dirección de destino (" + envios[0].direction + ") " + lib.parseDate(new Date(Date.now())),
                    size: 12
                };
                estados.push(e);
            }

            gestorBD.modificarEstado({code: Ecode}, estados, function (result) {
                if (result == null)
                    res.redirect("/envio/actualizar/" + envios[0].code + "?mensaje=Error del servidor&tipoMensaje=alert-danger");
                res.redirect("/envio/actualizar/" + envios[0].code + "?mensaje=Envio actualizado correctamente&tipoMensaje=alert alert-success");
            })
        });
    });

    app.get('/envios/agregar', function (req, res) {
        let params = [];
        res.send(lib.globalRender('views/bagregar.html', params, req.session));
    });

    app.post("/envio", function (req, res) {
            gestorBD.obtenerTransportistas({}, function (transportistas) {
                let trans = transportistas[Math.floor(Math.random() * transportistas.length)];
                let states = [];
                let date = new Date(Date.now());
                let estado = {
                    name: "En preparación " + lib.parseDate(date),
                    size: 1
                };
                states.push(estado);
                let envio = {
                    name: req.body.nombre,
                    surname: req.body.apellidos,
                    Rname: req.body.Rnombre,
                    Rsurname: req.body.Rapellidos,
                    RDNI: req.body.dni,
                    direction: req.body.direccion,
                    province: req.body.provincia,
                    state: states,
                    type: req.body.tipo,
                    pickup: req.body.recogida,
                    transport: trans._id,
                    sendToOffice: req.body.direccion.toString() == "",
                    code: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
                };
                if (envio.pickup == "oficina") {
                    let estado2 = {
                        name: "En oficina de origen " + lib.parseDate(date),
                        size: 3
                    };
                    envio.state.push(estado2);
                }

                gestorBD.insertarEnvio(envio, function (id) {
                    if (id == null) {
                        res.redirect("/envios/agregar?mensaje=Error del servidor&tipoMensaje=alert-danger")
                    } else {
                        let params = [];
                        params['code'] = envio.code;
                        res.send(lib.globalRender('views/bcodigo.html', params, req.session));
                    }
                });
            });
        }
    )
    ;

}
;
