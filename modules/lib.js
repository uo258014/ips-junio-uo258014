const swig = require('swig');

function globalRender(route, params, session) {
    //params['user'] = session.usuario;
    //params['role'] = session.role;
    //params['wallet'] = session.money;
    return swig.renderFile(route, params);
}

function parseDate(date){
    return date.toDateString()+" "+date.toTimeString().split(' ')[0];
};

module.exports = {
    globalRender: globalRender,
    parseDate: parseDate
};