// Módulos
const express = require('express');
const app = express();
const crypto = require('crypto');

const rest = require('request');
app.set('rest', rest);

const jwt = require('jsonwebtoken');
app.set('jwt', jwt);

const http = require('http');

const expressSession = require('express-session');
app.use(expressSession({
    secret: 'abcdefg',
    resave: true,
    saveUninitialized: true
}));

const fileUpload = require('express-fileupload');
app.use(fileUpload());
const swig = require('swig');
const mongo = require('mongodb');


const gestorBD = require("./modules/gestorBD.js");
gestorBD.init(app, mongo);

app.use(express.static('public'));
// Variables
app.set('port', 8081);
app.set('db', 'mongodb://admin:sdi@tiendamusica-shard-00-00-48dwf.mongodb.net:27017,tiendamusica-shard-00-01-48dwf.mongodb.net:27017,tiendamusica-shard-00-02-48dwf.mongodb.net:27017/test?ssl=true&replicaSet=tiendamusica-shard-0&authSource=admin&retryWrites=true');
app.set('clave', 'abcdefg');
app.set('crypto', crypto);

//Rutas/controladores por lógica
require("./routes/renvios.js")(app, swig, gestorBD); // (app, param1, param2, etc.)


app.get('/', function (req, res) {
    res.redirect('/envios/agregar');
});


http.createServer(app).listen(app.get('port'), function () {
    console.log("Servidor activo");
});

